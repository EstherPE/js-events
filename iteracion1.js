// 1.1 Añade un botón a tu html con el id btnToClick y en tu javascript añade el 
// evento click que ejecute un console log con la información del evento del click

// 1.2 Añade un evento 'focus' que ejecute un console.log con el valor del input.

// 1.3 Añade un evento 'input' que ejecute un console.log con el valor del input.


function addButton (){
     let but = document.createElement('button');
     but.className='btnToClick';
     but.textContent='Pulsar';
     document.body.appendChild(but);
}
 
addButton();

const $$button = document.body.querySelector('.btnToClick');

$$button.addEventListener('click',clicar);


function clicar(){
    const $$p = document.createElement('p');
    $$p.innerHTML="Se ha pulsado el boton." ;
    document.body.appendChild($$p);
    const $$input = document.querySelectorAll('input');
    for (item of $$input){
        if(item.value!=""){
            const $$p = document.createElement('p');
            $$p.innerHTML="texto añadido: "+item.value ;
            document.body.appendChild($$p);
        }
    }
}

const $$focus = document.body.querySelector('.focus');
$$focus.addEventListener('focus',focusear);

function focusear() {
    const $$p = document.createElement('p');
    $$p.innerHTML="Se ha hecho focus en el segundo input." ;
    document.body.appendChild($$p);
}

const $$value = document.body.querySelector('.value');
$$value.addEventListener('input',inputE);

function inputE() {
    let $$n= document.querySelector('.value');
    const $$p = document.createElement('p');
    $$p.innerHTML="Se ha escrito en el tercer input: "+ $$n.value;
    document.body.appendChild($$p);
}